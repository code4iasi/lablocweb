(function() {
    'use strict';

    angular.module('app.administrator', [
        'app.core',
        'app.widgets'
    ]);
})();
