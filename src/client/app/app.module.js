(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.widgets',
        'app.login',
        'app.administrator',
        'app.owner',
        'app.dashboard',
        'app.layout'
    ]);

})();
