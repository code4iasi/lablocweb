(function() {
    'use strict';

    angular.module('app.owner', [
        'app.core',
        'app.widgets'
    ]);
})();
