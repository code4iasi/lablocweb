(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[LaBloc Error] ',
        appTitle: 'La Bloc',
        //apiUrl: 'http://apilabloc.nimasoftware.com/api'
        apiUrl: 'http://lablocapi.nimasoftware.com/api'
        //apiUrl: 'https://sheetsu.com/apis/d78b88c8'
    };

    core.value('config', config);

    core.config(configure);

    /* @ngInject */
    function configure($logProvider, routerHelperProvider, exceptionHandlerProvider) {
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({docTitle: ''});
    }


    core.config(['ngDialogProvider', function (ngDialogProvider) {
        ngDialogProvider.setDefaults({
            showClose: true,
            closeByDocument: false,
            closeByEscape: false
        });
    }]);
})();
